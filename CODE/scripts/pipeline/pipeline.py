import os
import cv2
import torch
import shutil
import numpy as np
from PIL import Image
import scipy.stats as sps
from torch.utils import data
import matplotlib.pyplot as plt
from scripts.network.network import DaphDataset
from sklearn.model_selection import train_test_split
from scripts.quality_estimator.quality_estimator import  MarkupIterator, QualityEstimator, draw_crosses


WARNING = "unable to read camera feed"
NON_WARNING = "camera video can be read"

n1 = 25
n2 = 25
PAIRS_MATRIX = [[(i, j) for i in range(n1)] for j in range(n2)]

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------

def stretch_gaussian(a, b):
    heatmap = np.zeros_like(PAIRS_MATRIX)
    mean = [len(PAIRS_MATRIX) // 2, len(PAIRS_MATRIX) // 2]
    cov=[[a, 0], [0, b]]
    heatmap = sps.multivariate_normal(mean=mean, cov=cov).pdf(PAIRS_MATRIX)
    return np.round(heatmap / heatmap.max() * 255).astype(np.uint8)


def rotation(image, angle):
    image_center = tuple((np.array(image.shape[1::-1]) // 2).astype(np.float32))
    angle = -angle / np.pi * 180
    angle = angle - angle // 360 * 360
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result


def drow_img(background, contour, rows, cols, return_max=False):
    contour_rows = contour.shape[0]
    contour_cols = contour.shape[1]

    new_background = np.zeros((background.shape[0] + contour_rows//2,
                               background.shape[1] + contour_cols//2))
    new_background[contour_rows//2:, contour_cols//2:] = background

    flap = new_background[rows: rows + contour_rows, cols: cols + contour_cols]

    if return_max:
        np.maximum.reduce([flap, contour], out=flap, axis=0)
    else:
        flap[contour > 0] = 0
        flap += contour

    background = new_background[contour_rows//2:, contour_cols//2:]

    return background


def markup_to_frame(markup_iterator, frame_rows=1024, frame_cols=1280):
    output = []
    for current_frame in markup_iterator:
        canvas = np.zeros((frame_rows, frame_cols), dtype=np.uint8)
        current_ellipses = current_frame.get_ellipses().ellipses
        for ellipse in current_ellipses:
            flap = stretch_gaussian(ellipse.a, ellipse.b)
            flap = rotation(flap, ellipse.orientation)
            canvas = drow_img(canvas, flap, ellipse.y0, ellipse.x0, return_max=True)
        output.append([current_frame.get_name(), canvas])
    return output


def create_data(MARKUP_PATHES, FRAMES_PATHES, pack_size, packs_multiplication, mode):
    packs = []
    for frames_path, murkup_path in zip(FRAMES_PATHES, MARKUP_PATHES):
        marked_frames = sorted(markup_to_frame(MarkupIterator(murkup_path)), key=lambda x: x[0])
        for marked_frame in marked_frames:
            marked_frame.append(cv2.imread(os.path.join(frames_path, marked_frame[0])))
        counter = 0
        while counter < len(marked_frames):
            if marked_frames[counter][-1] is None:
                marked_frames.pop(counter)
            else:
                counter += 1
            
            
        if mode == "sequential":
            for i in range(len(marked_frames) - pack_size + 1):
                packs.append([marked_frames[i + j] for j in range(pack_size)])
    
        if mode == "random":
            for i in range(packs_multiplication * len(marked_frames)):
                numbers = np.random.randint(len(marked_frames), size=pack_size)
                packs.append([marked_frames[j] for j in numbers])
    return packs


def post_data(MARKUP_PATHES, FRAMES_PATHES, OUT_PATH, format=".png", pack_size=4, packs_multiplication=1, mode="random"):
    packs = create_data(MARKUP_PATHES, FRAMES_PATHES, pack_size, packs_multiplication, mode)
    
    if os.path.exists(OUT_PATH):
        shutil.rmtree(OUT_PATH)
    os.makedirs(OUT_PATH)
    
    for i, pack in enumerate(packs):
        pack_path = os.path.join(OUT_PATH, f"pack_{i}")
        frames_path = os.path.join(pack_path, "frames")
        gaussians_path = os.path.join(pack_path, "gaussians")

        os.makedirs(pack_path)
        os.makedirs(frames_path)
        os.makedirs(gaussians_path)
        
        for j, (name, gaussian, frame) in enumerate(pack):
            cv2.imwrite(os.path.join(frames_path, f"{j}"+format), frame[:, :, 0])
            cv2.imwrite(os.path.join(gaussians_path, f"{j}"+format), gaussian)

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------

def inputs_targets_pathes(train_dirs, FRAMES, HEATMAPS):
    inputs, targets = [], []
    for train_dir in train_dirs:
        for pack_name in os.listdir(train_dir):
            if "pack" not in pack_name:
                continue
            pack = [[], []]
            pack_name = os.path.join(train_dir, pack_name)
            for frame_name in os.listdir(os.path.join(pack_name, FRAMES)):
                if ".png" in frame_name:
                    pack[0].append(os.path.join(pack_name, FRAMES, frame_name))

            for heatmap_name in os.listdir(os.path.join(pack_name, HEATMAPS)):
                if ".png" in heatmap_name:
                    pack[1].append(os.path.join(pack_name, HEATMAPS, heatmap_name))

            pack[0] = sorted(pack[0])
            pack[1] = sorted(pack[1])

            inputs.append (pack[0])
            targets.append(pack[1])
    return inputs, targets


def create_train_valid_dataloader(inputs, targets, minibatch_size, train_size = 0.9):
    random_seed = 42

    inputs_train, inputs_valid   = train_test_split(inputs,
                                                    random_state=random_seed,
                                                    train_size=train_size,
                                                    shuffle=True)
    targets_train, targets_valid = train_test_split(targets,
                                                    random_state=random_seed,
                                                    train_size=train_size,
                                                    shuffle=True)

    dataset_train = DaphDataset(inputs=inputs_train, 
                                targets=targets_train)
    dataset_valid = DaphDataset(inputs=inputs_valid, 
                                targets=targets_valid)

    dataloader_train = data.DataLoader(dataset=dataset_train,
                                       batch_size=minibatch_size,
                                       shuffle=True)

    dataloader_valid = data.DataLoader(dataset=dataset_valid,
                                       batch_size=minibatch_size,
                                       shuffle=False)
    return dataloader_train, dataloader_valid

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------

def video_reader(video_path):
    cap = cv2.VideoCapture(video_path)
    print(WARNING) if (cap.isOpened() == False) else print(NON_WARNING)

    try:
        ret = True
        while ret:
            ret, frame = cap.read()
            if ret:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                yield (True, frame)
    except StopIteration:
        cap.release()
        cv2.destroyAllWindows()
        return False, None


def frame_processor(model):
    try:
        ret, frame = yield None
        
        while ret:
            ellipses = torch.sigmoid(model.forward(frame)).detach().numpy()
            ret, frame = yield ellipses
    except StopIteration:
        pass


def pipeline(video_pipe, processor_pipe, amount, replication, threshold, replication_batch_size=12, pack_size=4):
    out = []
    replication_batch_size = max(min(amount, replication_batch_size), 2)
    
    while amount > 0:
    
    #набираем батч
        frames = []
        for i, (ret, frame) in zip(range(replication_batch_size), video_pipe):
            frames.append(frame)
            if len(frames) != replication_batch_size:
                continue

        frames = np.array(frames)
        replication_batch_size = len(frames)
        amount -= replication_batch_size

    #составляем четверки
        one_frame_amount = (replication + pack_size - 1) // pack_size
        order = np.ones(replication_batch_size * one_frame_amount, dtype=np.int32)
        for i in range(replication_batch_size):
            order[i * one_frame_amount : (i + 1) * one_frame_amount] = i
        np.random.shuffle(order)

    #пропускаем четверки через нейронку
        sum_ellipses = [None for i in range(replication_batch_size)]
        for i in range(len(order)):
            frames_pack = [frames[order[k % len(order)]] for k in range(i, i + pack_size)]
            frames_pack = torch.Tensor(np.array(frames_pack)[np.newaxis])

            ellipses = processor_pipe.send((True, frames_pack))
            ellipses = np.uint8((ellipses * 255) > (threshold * 255)) * 255

            for j in range(pack_size):
                k = i + j
                index = order[k % len(order)]
                if sum_ellipses[index] is None:
                    sum_ellipses[index] = ellipses[0, j]
                    continue
                sum_ellipses[index] = np.maximum(sum_ellipses[index], ellipses[0, j])

    #отсеиваем лишние точки
        for frame, processed_frame in zip(frames, sum_ellipses):
            points = QualityEstimator(processed_frame, None).points
            frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
            frame = draw_crosses(image=frame, points=points, size=5, 
                                 vertical_border=frame.shape[0], 
                                 horizontal_border=frame.shape[1],
                                 channel=1)
            out.append(frame)
    return np.array(out)


def save_generated_frames(generated_frames, output_path, suffix, amount):
    frame_width = int(generated_frames[0].shape[1])
    frame_height = int(generated_frames[0].shape[0])
    fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
    out = cv2.VideoWriter(os.path.join(output_path, suffix), fourcc, 24, (frame_width, frame_height))

    
    for frame in generated_frames:
        out.write(frame)
        amount -= 1

    out.release()
    cv2.destroyAllWindows()