import torch
import torch.nn as nn

import gc
import time
import torch.optim as optim

import os
import numpy as np
from PIL import Image
from shutil import rmtree
from skimage.io import imread


class WeightedNeighborhoodBCELoss(nn.Module):
    def __init__(self, device, pos_weight=1.0, bdr_weight=1.0, bce_l=0.5, bce_u=0.99):
        super(WeightedNeighborhoodBCELoss, self).__init__()
        self.pos_weight = torch.Tensor([pos_weight])
        self.bdr_weight = torch.Tensor([bdr_weight])
        self.bce_l = bce_l
        self.bce_u = bce_u
        self.device = device

    def forward(self, outputs, labels):
        labels_neg = (labels <= self.bce_l).type(torch.FloatTensor).to(self.device)
        labels_pos = (labels >= self.bce_u).type(torch.FloatTensor).to(self.device)

        mask_1 = (labels > self.bce_l).type(torch.FloatTensor).to(self.device)
        mask_2 = (labels < self.bce_u).type(torch.FloatTensor).to(self.device)
        labels_bdr = mask_1 * mask_2

        pos_weight = self.pos_weight.to(self.device)
        bdr_weight = self.bdr_weight.to(self.device)

        loss_pos = (outputs + 1e-5).log() * labels_pos * pos_weight
        loss_bdr = (1 - outputs + 1e-5).log() * labels_bdr * bdr_weight
        loss_neg = (1 - outputs + 1e-5).log() * labels_neg

        l = -(loss_pos + loss_neg + loss_bdr)
        return l.mean()




def training_loop(net, device,
                  pos_weight, border_weight, bce_l, bce_u,
                  learning_rate, momentum,
                  epoches,
                  dataloader_train, dataloader_valid,
                  checkpoint_dir, last_chkpnt_path=None):
    model = net()
    model = model.to(device)
    if last_chkpnt_path is not None:
        model.load_state_dict(torch.load(last_chkpnt_path, map_location=torch.device(device)))

    criterion = WeightedNeighborhoodBCELoss(device, pos_weight, border_weight, bce_l, bce_u)
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum, weight_decay=1e-5)

    for epoch in range(0, epoches):
        start = time.time()

        if epoch != 0:
            model.load_state_dict(torch.load(last_chkpnt_path))

        model.train()
        train_loss = 0.0
        train_iters = 0
        for i, data in enumerate(dataloader_train, 0):
            inputs, labels = data
            if len(inputs.shape) == 3:
                inputs = inputs.unsqueeze(1)
                labels = labels.unsqueeze(1)
            inputs, labels = inputs.to(device), labels.to(device)

            optimizer.zero_grad()
            outputs = model(inputs)
            outputs = torch.sigmoid(outputs)
            loss = criterion.forward(outputs, labels)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            train_iters += 1

        model.eval()
        valid_loss = 0.0
        valid_iters = 0
        with torch.no_grad():
            for i, data in enumerate(dataloader_valid, 0):
                inputs, labels = data

                if len(inputs.shape) == 3:
                    inputs = inputs.unsqueeze(1)
                    labels = labels.unsqueeze(1)
                inputs, labels = inputs.to(device), labels.to(device)

                outputs = model(inputs)
                outputs = torch.sigmoid(outputs)
                loss = criterion.forward(outputs, labels)

                valid_loss += loss.item()
                valid_iters += 1

        chkpnt_path = os.path.join(checkpoint_dir, str(epoch + 1) + '.chkpnt')
        torch.save(model.state_dict(), chkpnt_path)
        last_chkpnt_path = chkpnt_path

        end = time.time()
        print(
            f'[{epoch + 1}/{epoches}] train loss: {train_loss / train_iters:.7f}, valid loss: {valid_loss / valid_iters:.7f} ({(end - start):.2f}s)')

    del outputs
    del loss
    if device == "cuda":
        torch.cuda.empty_cache()
    gc.collect()
    with torch.no_grad():
        if device == "cuda":
            torch.cuda.empty_cache()
    print('Finished Training')

    return last_chkpnt_path




def create_dir(dir_path):
    if os.path.exists(dir_path):
            rmtree(dir_path)

    os.makedirs(dir_path)


def extract_image_names(dir_path, type=".png"):
    names  = []
    pathes = []
    
    for img_name in os.listdir(dir_path):
        names.append(img_name.split(".")[0])
    names  = sorted(names, key=lambda x: int(x))
    
    for name in names:
        pathes.append(os.path.join(dir_path, name+type))

    return names, pathes


# def assemble_packages(names, pack_size, randomise_pack=False, packet_multiplication=4):
#     packs = []
#     names = np.array(names)
#     if randomise_pack:
#         for _ in range(len(names) * packet_multiplication):
#             indexes = np.random.randint(len(names), size=pack_size)
#             packs.append(names[indexes])
#     else:
#         packs = [[names[j + i] for i in range(pack_size)] 
#                   for j in range(len(names) - pack_size + 1)]
#     return packs


def assemble_packages(names, pack_size=4, randomise_pack=False, replication=12):
    packs = []
    names = np.array(names)
    amount = len(names)
    
    if randomise_pack:
        one_frame_amount = (replication + pack_size - 1) // pack_size
        order = np.ones(amount * one_frame_amount, dtype=np.int32)
        
        for i in range(amount):
            order[i * one_frame_amount : (i + 1) * one_frame_amount] = i
        np.random.shuffle(order)

        for i in range(len(order)):
            names_pack = [names[order[k % len(order)]] for k in range(i, i + pack_size)]
            packs.append(names_pack)
    else:
        packs = [[names[j + i] for i in range(pack_size)] 
                  for j in range(len(names) - pack_size + 1)]
    return packs


def process_and_save_pack(pack, images_arrays, model, out, device, format, threshold=0.5):
    current_arrays = [images_arrays[image_name] for image_name in pack]

#     frames_pack = torch.Tensor(np.array(frames_pack)[np.newaxis])
#     ellipses = torch.sigmoid(model.forward(frame)).detach().numpy()
    
#     input_data = np.array(current_arrays).astype(np.float32)# / 255.0
#     input_data = torch.from_numpy(input_data).type(torch.float32)
#     input_data = input_data.unsqueeze(0).to(device)
    
    input_data = torch.Tensor(np.array(current_arrays)[np.newaxis]).to(device)
    predictions = torch.sigmoid(model.forward(input_data)).detach().cpu().numpy()

    for j, name in enumerate(pack):
        prediction = np.uint8(predictions[0, j, :] * 255)
        prediction = np.uint8(prediction > (threshold * 255)) * 255
        prediction = Image.fromarray(prediction, 'L')
        prediction.save(os.path.join(out, name, f"{len(os.listdir(os.path.join(out, name)))}"+format))


def aggregate_pack(img_name, out, out_sum, format):
    current_path = os.path.join(out, img_name)
    arrays = [imread(os.path.join(current_path, name)) for name in os.listdir(current_path)]
    prediction = np.maximum.reduce(arrays)
    prediction = Image.fromarray(prediction, 'L')
    prediction.save(os.path.join(out_sum, img_name + format))


def test_loop(net, device, run_dirs, pack_size, last_chkpnt_path=None, format=".png", randomise_pack=False, replication=1, threshold=0.5):
    model = net()
    model = model.to(device)
    model.load_state_dict(torch.load(last_chkpnt_path, map_location=torch.device(device)))

    for rd in run_dirs:
        test_dir = rd[0]
        out_dir = rd[1]

        out_dir_prd     = os.path.join(out_dir, 'prd')
        out_dir_prd_sum = os.path.join(out_dir, 'sum_prd')

        create_dir(out_dir_prd)
        create_dir(out_dir_prd_sum)

        image_names, image_pathes = extract_image_names(test_dir)
        image_packs = assemble_packages(image_names, pack_size, randomise_pack, replication)

        for name in image_names:
            os.makedirs(os.path.join(out_dir_prd, name))

        images_arrays = {name:imread(path) for name, path in zip(image_names, image_pathes)}
        for pack in image_packs:
            process_and_save_pack(pack, images_arrays, model, out_dir_prd, device, format, threshold)

        for image_name in image_names:
            aggregate_pack(image_name, out_dir_prd, out_dir_prd_sum, format)