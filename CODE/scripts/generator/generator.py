import os
import cv2
import shutil
import numpy as np
from PIL import Image


#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
#------------------------------------EMULATOR------------------------------------
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------


def rotation(image, angle):
    image_center = tuple((np.array(image.shape[1::-1]) // 2).astype(np.float32))
    angle = -angle / np.pi * 180
    angle = angle - angle // 360 * 360
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result


def drow_img(background, contour, rows, cols, return_max=False):
    contour_rows = contour.shape[0]
    contour_cols = contour.shape[1]

    flap = background[rows: rows + contour_rows, cols: cols + contour_cols]
    if return_max:
        np.maximum.reduce([flap, contour], out=flap, axis=0)
    else:
        flap[contour > 0] = 0
        flap += contour

    return background


def get_next_state(background,
                   contours, heatmaps,
                   rows, cols, phis,
                   row_steps, col_steps,
                   row_crop, col_crop,
                   margin):
    max_rotation = np.pi / 6
    max_step = 10
    ellipse_threshold = 10

    rotated_heatmaps = np.array([rotation(np.copy(heatmaps[i]), phi) for i, phi in enumerate(phis)])
    rotated_heatmaps = rotated_heatmaps[:, row_crop[0]:row_crop[1], col_crop[0]:col_crop[1]]
    rotated_ellipses = (rotated_heatmaps > ellipse_threshold).astype(np.uint8) * 255
    rotated_contours = np.array([rotation(np.copy(contours[i]), phi) for i, phi in enumerate(phis)])
    rotated_contours = rotated_contours[:, row_crop[0]:row_crop[1], col_crop[0]:col_crop[1]]
    rotated_contours[rotated_contours > 135] = 0

    rows += row_steps
    cols += col_steps

    rounded_rows = np.round(rows, 0).astype(np.int64)
    rounded_cols = np.round(cols, 0).astype(np.int64)

    rounded_rows[rounded_rows < margin[0]] = margin[0]
    rounded_cols[rounded_cols < margin[1]] = margin[1]
    rounded_rows[rounded_rows >= (background.shape[0] - rotated_ellipses[0].shape[0]) - margin[2]] = \
        background.shape[0] - rotated_ellipses[0].shape[0] - margin[2] - 1
    rounded_cols[rounded_cols >= (background.shape[1] - rotated_ellipses[0].shape[1]) - margin[3]] = \
        background.shape[1] - rotated_ellipses[0].shape[1] - margin[3] - 1

    delta_phi = np.random.uniform(-1 * max_rotation, max_rotation, size=len(rotated_contours))
    phis += delta_phi
    steps = np.random.uniform(max_step, size=len(rotated_contours))
    row_steps = np.sin(phis) * steps
    col_steps = np.cos(phis) * steps

    rows = rounded_rows.astype(np.float64)
    cols = rounded_cols.astype(np.float64)

    return (rotated_contours, rotated_ellipses, rotated_heatmaps), (rows, cols, phis), (row_steps, col_steps)


def emulator(n_frames, n_daphnia, contour_examples, heatmap_examples, background, margin, create_actions=False):
    frames = []
    masks = []
    gaussians = []
    contour_rows = contour_examples[0].shape[0] // 3
    contour_cols = contour_examples[0].shape[1] // 3
    row_crop = [contour_rows, 2 * contour_rows]
    col_crop = [contour_cols, 2 * contour_cols]

    # selected dafnii
    order = np.arange(0, n_daphnia, 1)
    contours = np.array([contour_examples[i] for i in np.random.randint(len(contour_examples), size=n_daphnia)])
    heatmaps = np.array([heatmap_examples[i] for i in np.random.randint(len(heatmap_examples), size=n_daphnia)])
    # initial row for each dafni
    rows = np.random.uniform(background.shape[0] - contour_rows - margin[0] - margin[2], size=n_daphnia) + margin[0]
    # initial col for each dafni
    cols = np.random.uniform(background.shape[1] - contour_cols - margin[1] - margin[3], size=n_daphnia) + margin[1]
    # initial rotation about the horizontal axis counterclockwise for each dafni
    phis = np.random.uniform(2 * np.pi, size=n_daphnia)

    # the order of first step for each dafni
    row_steps = np.sin(phis)
    col_steps = np.cos(phis)

    for _ in range(n_frames):
        current_frame = np.copy(background)
        current_mask_frame = np.zeros_like(background)
        current_gaussian_frame = np.zeros_like(background)

        current_parameters = get_next_state(background, contours, heatmaps, rows, cols, phis, row_steps, col_steps,
                                            row_crop, col_crop, margin)
        current_rotations = current_parameters[0]
        current_coordinates = current_parameters[1]
        current_order = current_parameters[2]

        rotated_contours, rotated_ellipses, rotated_heatmaps = current_rotations
        rows, cols, phis = current_coordinates
        row_steps, col_steps = current_order

        rounded_rows = rows
        rounded_cols = cols
        data = zip(rotated_contours, rotated_ellipses, rotated_heatmaps, rows, cols)
        for contour, ellipse, heatmap, r_rows, r_cols in data:
            r_rows, r_cols = int(r_rows), int(r_cols)
            current_frame = drow_img(current_frame, contour, r_rows, r_cols)
            current_mask_frame = drow_img(current_mask_frame, ellipse, r_rows, r_cols)
            current_gaussian_frame = drow_img(current_gaussian_frame, heatmap, r_rows, r_cols, return_max=True)

        frames.append(current_frame)
        masks.append(current_mask_frame)
        gaussians.append(current_gaussian_frame)

        if create_actions:
            np.random.shuffle(order)
            contours = contours[order]
            # ellipses = ellipses[order]
            heatmaps = heatmaps[order]

    return frames, masks, gaussians


#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------


def save_generated_frames(generated_frames, output_path, suffix):
    frame_width = int(generated_frames[0].shape[1])
    frame_height = int(generated_frames[0].shape[0])
    fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
    out = cv2.VideoWriter(os.path.join(output_path, suffix), fourcc, 24, (frame_width,frame_height))

    for frame in generated_frames:
        bgr_frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        out.write(bgr_frame)

    out.release()
    cv2.destroyAllWindows()


def save_frames(frames, output_path, suffix):
    frame_width  = int(frames[0].shape[1])
    frame_height = int(frames[0].shape[0])
    fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
    out = cv2.VideoWriter(os.path.join(output_path, suffix), fourcc, 24, (frame_width,frame_height))

    for frame in frames:
        out.write(frame)

    out.release()
    cv2.destroyAllWindows()
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

def generate_packs(output_path,
                   contour_examples, heatmap_examples, background_examples,
                   margins,
                   pack_size=4, packs_amount=100, n_daphnia=100,
                   image_type=".png",
                   create_actions=False, 
                   randomise_pack=False):
    if os.path.exists(output_path):
        shutil.rmtree(output_path)
    os.makedirs(output_path)

    types = ["frames", "masks", "gaussians"]
    for i in range(packs_amount):
        current_path = os.path.join(output_path, f"pack_{i}")
        background_number = np.random.randint(len(background_examples))

        generated_frames = []
        generated_masks = []
        generated_gaussians = []
        if randomise_pack:
            for _ in range(pack_size):
                generated_frame, generated_mask, generated_gaussian = emulator(1, n_daphnia,
                                                                               contour_examples, heatmap_examples,
                                                                               background_examples[background_number],
                                                                               margins[background_number],
                                                                               create_actions)
                generated_frames.extend(generated_frame)
                generated_masks.extend(generated_mask)
                generated_gaussians.extend(generated_gaussian)
                
        else:        
            generated_frames, generated_masks, generated_gaussians = emulator(pack_size, n_daphnia,
                                                                              contour_examples, heatmap_examples,
                                                                              background_examples[background_number],
                                                                              margins[background_number],
                                                                              create_actions)
        
        generated_frames = np.array(generated_frames)
        generated_masks = np.array(generated_masks)
        generated_gaussians = np.array(generated_gaussians)
        
        if os.path.exists(current_path):
            shutil.rmtree(current_path)
        os.makedirs(current_path)

        for type in types:
            generated = None
            current_type_path = os.path.join(current_path, type)
            os.makedirs(current_type_path)

            if type == types[0]:
                generated = generated_frames
            if type == types[1]:
                generated = generated_masks
            if type == types[2]:
                generated = generated_gaussians

            for j in range(pack_size):
#                 rgb = np.dstack((generated[j], generated[j], generated[j]))
#                 rgb = Image.fromarray(rgb, 'RGB')
#                 rgb.save(os.path.join(current_type_path, str(j) + image_type))
                frame = Image.fromarray(generated[j], 'L')
                frame.save(os.path.join(current_type_path, str(pack_size * i + j) + image_type))


def generate_dataset(output_path,
                     contour_examples, heatmap_examples, background_examples,
                     margins,
                     pack_size=4, dataset_size=4, n_daphnia=100,
                     image_type=".png",
                     create_actions=False):
    if os.path.exists(output_path):
        shutil.rmtree(output_path)
    os.makedirs(output_path)

    types = ["frames", "masks", "gaussians"]
    for i in range(dataset_size // pack_size):
        background_number = np.random.randint(len(background_examples))
        generated_frames, generated_masks, generated_gaussians = emulator(pack_size, n_daphnia,
                                                                          contour_examples, heatmap_examples,
                                                                          background_examples[background_number],
                                                                          margins[background_number],
                                                                          create_actions)

        for type in types:
            generated = None
            current_type_path = os.path.join(output_path, type)
            if not os.path.exists(current_type_path):
                os.makedirs(current_type_path)

            if type == types[0]:
                generated = generated_frames
            if type == types[1]:
                generated = generated_masks
            if type == types[2]:
                generated = generated_gaussians

            for j in range(pack_size):
#                 rgb = np.dstack((generated[j], generated[j], generated[j]))
#                 rgb = Image.fromarray(rgb, 'RGB')
#                 rgb.save(os.path.join(current_type_path, str(4 * i + j) + image_type))
                frame = Image.fromarray(generated[j], 'L')
                frame.save(os.path.join(current_type_path, str(pack_size * i + j) + image_type))

