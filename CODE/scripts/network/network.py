import torch
import numpy as np
from torch.utils import data
from skimage.io import imread

import torch.nn as nn
import torch.nn.functional as F
import torchvision


class DaphDataset(data.Dataset):
    def __init__(self, inputs: list, targets: list, transform=None):
        self.inputs = inputs
        self.targets = targets
        self.inputs_dtype = torch.float32
        self.targets_dtype = torch.float32

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, index: int):
        input_pack_ID = self.inputs[index]
        target_pack_ID = self.targets[index]

        x = np.vstack([(imread(input_ID ).astype(np.float32) / 255.0)[np.newaxis] for input_ID  in input_pack_ID])
        y = np.vstack([(imread(target_ID).astype(np.float32) / 255.0)[np.newaxis] for target_ID in target_pack_ID])
        
        x = torch.from_numpy(x).type(self.inputs_dtype )
        y = torch.from_numpy(y).type(self.targets_dtype)
        return x, y




class Block(nn.Module):
    def __init__(self, input_channels, output_channels):
        super().__init__()
        self.conv1 = nn.Conv2d(input_channels,  output_channels, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(output_channels, output_channels, kernel_size=3, stride=1, padding=1)
        self.relu = nn.ReLU()


    def forward(self, x):
        return self.relu(self.conv2(self.relu(self.conv1(x))))




class Encoder(nn.Module):
    def __init__(self, channels_order=(1, 8, 16, 32)):
        super().__init__()

        in_out_iteraior = zip(channels_order[:-1], channels_order[1:])
        self.blocks = nn.ModuleList([Block(in_, out_) for (in_, out_) in in_out_iteraior])
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)


    def forward(self, x):
        features = []
        for block in self.blocks:
            x = block(x)
            features.append(x)
            x = self.pool(x)
        return features


    

class Decoder(nn.Module):
    def __init__(self, channels_order=(32, 16, 8, 1)):
        super().__init__()
        in_out_iteraior = list(zip(channels_order[:-1], channels_order[1:]))
        self.upconvs = nn.ModuleList([nn.ConvTranspose2d(in_, out_, 2, 2) for (in_, out_) in in_out_iteraior])
        self.blocks  = nn.ModuleList([Block(in_, out_) for (in_, out_) in in_out_iteraior])


    def forward(self, x, encoder_features):
        for i, (upconv, block) in enumerate(zip(self.upconvs[:-1], self.blocks[:-1])):
            x = block(torch.cat([upconv(x), encoder_features[i]], dim=1))
        x = self.blocks[-1](x)
        return x



class UNet(nn.Module):
    def __init__(self, 
                 input_channels=4,
                 output_channels=4,
                 encoder_channels=(4, 8, 16, 32),
                 decoder_channels=(32, 16, 8, 4)):
        super().__init__()
        self.in_conv  = nn.Conv2d(input_channels,  input_channels,  kernel_size=1)
        self.encoder = Encoder(encoder_channels)
        self.decoder = Decoder(decoder_channels)
        self.out_conv = nn.Conv2d(2 * output_channels, output_channels, kernel_size=1)


    def forward(self, x):
        x = self.in_conv(x)
        input_features = torch.clone(x)
        encoder_features = self.encoder.forward(x)
        out = self.decoder.forward(encoder_features[::-1][0], encoder_features[::-1][1:])
        out = self.out_conv(torch.cat([out, input_features], dim=1))
        return out